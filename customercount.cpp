#include <iostream>
using namespace std;

class CustomerCounter {
public:
  int maximum_customers;
  int customer_count;

  CustomerCounter (int x){
    maximum_customers = 10;
    customer_count = customer_count + x;
  }
  void printcurrent(){
    cout<< "\n There are "<< customer_count<< " customers in the store right now";
  }
  
};

int main (){
 
  int userinp;
  cout << " \n Enter the number of customers entering the shop: ";
  cin >> userinp;

  CustomerCounter obj1(userinp);
  if (obj1.customer_count>obj1.maximum_customers){
    cout << "amount has exceeded the number of customers allowed in the shop.";
    obj1.printcurrent();
    
  }
  else{
    while (obj1.customer_count<=obj1.maximum_customers)
      {
	cout<< "\n Enter the number of customers that have entered or left the shop using the minus sign (-): ";
	cin >> userinp;
	obj1(userinp);
	obj1.printcurrent();
	
	
      }
  }

}

  
