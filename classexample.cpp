#include <iostream>
#include <string>
using namespace std;

class Cars {
  public:
  string brand;
  string model;
  string year;

public:
  void printout() {
    cout << " The car brand is " << brand << " and the model is " << model << " and it was made in " << year <<endl;
  }
};

  int main ()
 {
  Cars num1;
  Cars num2;

  num1.brand = "Chevy";
  num1.model = "Tahoe";
  num1.year = "2015";

  cout << num1.brand;

  num2.brand = "Lexus";
  num2.model = "IS350";
  num2.year = "2020";

  num1.printout();
  num2.printout();

  return 0;
  
}
